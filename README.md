# Basic Web Server Application
Simple web server application using Spring Boot (Spring Data JPA, Spring Web, Spring Security)

## Architecture
The application is based on a Layered Architecture which consists of 3 layers: Presentation Layer, Bussiness Logic Layer and Data Access Layer.

## Features
- Authentication and authorization filter using Spring Security and JWT
- Database management using MySQL
- Few HATEOAS implementation using Spring HATEOAS
- Unit testing was performed using JUnit 5 and Mockito
- Gradle was used as the build tool