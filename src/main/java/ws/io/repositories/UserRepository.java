package ws.io.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ws.io.entity.UserEntity;

import javax.transaction.Transactional;
import java.util.List;

// spring data jpa
@Repository
public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    // query methods
    UserEntity findByEmail(String email);
    UserEntity findByUserId(String userId);
    UserEntity findByEmailVerificationToken(String emailVerificationToken);

    // Native Sql Queries

    // count query added to support pagination, but is optional,
    // in which case spring data jpa will figure out what will be the total of elements to be splitted out
    @Query(value ="select * from Users u where u.email_verification_status = true",
            countQuery ="select count(*) from Users u where u.email_verification_status = 'true'",
            nativeQuery = true)
    Page<UserEntity> findAllWithConfirmedEmailAddress(Pageable pageableRequest);

    // Positional/index Parameters
    @Query(
            value = "select * from Users u where u.first_name = ?1 and u.last_name=?2",
            nativeQuery = true)
    List<UserEntity> findUserByName(String firstName, String lastName);

    // Named Parameters
    @Query(
            value = "select * from Users u where u.first_name = :firstN and u.last_name= :lastN and u.email= :email",
            nativeQuery = true)
    List<UserEntity> findUserByNameAndEmail(@Param("firstN") String firstName, @Param("lastN") String lastName,
                                            @Param("email") String email);

    @Query(
            value = "select u.first_name, u.last_name from Users u where u.last_name LIKE %:keyword",
            nativeQuery = true)
    List<Object[]> findUsersByKeyword(@Param("keyword") String keyword);

    @Transactional
    @Modifying
    @Query(
            value = "update users u set u.email_verification_status=:emailVerificationStatus where u.user_id=:userId",
            nativeQuery = true)
    void updateUserEmailVerificationStatus(@Param("emailVerificationStatus") boolean emailVerificationStatus,
                                           @Param("userId") String userId);


    // Java Persistence Query Language (JPQL) Queries

    @Query("select user from UserEntity user where user.userId=:userId")
    UserEntity findUserEntityByUserId(@Param("userId") String userId);

    @Query("select user.firstName, user.lastName from UserEntity user where user.userId=:userId")
    List<Object[]> getUserFullNameById(@Param("userId") String userId);

    @Transactional
    @Modifying
    @Query("update UserEntity u set u.emailVerificationStatus=:emailVerificationStatus where u.userId=:userId")
    void updateUserEmailVerificationStatusJPQL(@Param("emailVerificationStatus") boolean emailVerificationStatus,
                                               @Param("userId") String userId);

}
