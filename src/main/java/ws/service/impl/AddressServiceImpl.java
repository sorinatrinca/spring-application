package ws.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ws.io.entity.AddressEntity;
import ws.io.entity.UserEntity;
import ws.io.repositories.AdressRepository;
import ws.io.repositories.UserRepository;
import ws.service.AddressService;
import ws.shared.dto.AddressDto;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AdressRepository addressRepository;

    @Override
    public List<AddressDto> getAddresses(String userId) {
        List<AddressDto> returnValue = new ArrayList<>();
        ModelMapper modelMapper = new ModelMapper();

        UserEntity userEntity = userRepository.findByUserId(userId);
        if (userEntity == null) return returnValue;

        Iterable<AddressEntity> addresses = addressRepository.findAllByUserDetails(userEntity);
        for(AddressEntity address: addresses){
            returnValue.add(modelMapper.map(address,AddressDto.class));
        }

        return returnValue;
    }

    @Override
    public AddressDto getAddress(String addressId) {
         AddressDto returnValue = null;
         AddressEntity addressEntity = addressRepository.findByAddressId(addressId);

         if (addressEntity !=null){
             returnValue = new ModelMapper().map(addressEntity,AddressDto.class);
         }

         return returnValue;
    }
}
