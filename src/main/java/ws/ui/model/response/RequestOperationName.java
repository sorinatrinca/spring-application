package ws.ui.model.response;

public enum RequestOperationName {
    DELETE,
    VERIFY_EMAIL
}
