package ws.io.repositories;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import ws.io.entity.UserEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void getUsersByConfirmedEmailStatus(){
        Pageable pageableRequest = PageRequest.of(0,2);
        Page<UserEntity> userEntityPage = userRepository.findAllWithConfirmedEmailAddress(pageableRequest);
        assertNotNull(userEntityPage);

        List<UserEntity> userEntities = userEntityPage.getContent();
        assertNotNull(userEntities);
        assertEquals(2,userEntities.size());
    }

    @Test
    void finUserByName(){
        List<UserEntity> users = userRepository.findUserByName("Sorina", "Trinca");
        assertNotNull(users);
        assertEquals(2, users.size());
        assertEquals("Sorina", users.get(0).getFirstName());
    }

    @Test
    void finUserByNameAndEmail(){
        List<UserEntity> users = userRepository.findUserByNameAndEmail("Flavia", "Trinca", "sorindhhddqwa@ffyadhoo.com");
        assertNotNull(users);
        assertEquals(1, users.size());
        assertEquals("flavia", users.get(0).getFirstName());
    }

    @Test
    void finUserByKeyword(){
        String keyword = "nca";

        List<Object[]> users = userRepository.findUsersByKeyword(keyword);
        assertNotNull(users);
        assertEquals(3, users.size());

        Object[] user = users.get(0);
        String firstName = String.valueOf(user[0]);
        String lastName = String.valueOf(user[1]);

        assertNotNull(firstName);
        assertNotNull(lastName);

        System.out.println(firstName+" "+lastName);
    }

    @Test
    void updateUserEmailVerificationStatus(){
        boolean newEmailStatus = true;
        userRepository.updateUserEmailVerificationStatus(newEmailStatus, "yfCtf8fEiAhgWtfnl5lYCJ8TESYgLK");

        UserEntity user = userRepository.findByUserId("yfCtf8fEiAhgWtfnl5lYCJ8TESYgLK");
        boolean status = user.getEmailVerificationStatus();

        assertEquals(newEmailStatus, status);
    }

    @Test
    void testJPQLFindUserById(){
        String userId = "yfCtf8fEiAhgWtfnl5lYCJ8TESYgLK";
        UserEntity user = userRepository.findUserEntityByUserId(userId);

        assertNotNull(user);
        assertEquals(userId, user.getUserId());
    }

    @Test
    void testJPQLGetUserFullNameByUserId(){
        String userId = "yfCtf8fEiAhgWtfnl5lYCJ8TESYgLK";
        List<Object[]> records = userRepository.getUserFullNameById(userId);

        assertNotNull(records);
        assertTrue(records.size() == 1);

        Object[] userDetails = records.get(0);

        String firstName = String.valueOf(userDetails[0]);
        String lastName = String.valueOf(userDetails[1]);

        assertNotNull(firstName);
        assertNotNull(lastName);
    }

    @Test
    void testJPQLUpdateUserEmailVerificationStatus(){
        boolean newEmailStatus = true;
        userRepository.updateUserEmailVerificationStatusJPQL(newEmailStatus, "yfCtf8fEiAhgWtfnl5lYCJ8TESYgLK");

        UserEntity user = userRepository.findByUserId("yfCtf8fEiAhgWtfnl5lYCJ8TESYgLK");
        boolean status = user.getEmailVerificationStatus();

        assertEquals(newEmailStatus, status);
    }
}